# GFXRC_Generator

### About
	This project extracts GFX_RUNTIME_CLASS information from sro_client.
	It will then generate class hierarchy based structures for all of the classes found.
	These generated structures can be further reversed/labeled and used in other projects alongside the generated json file.
	In addition, an x32dbg label script is generated to apply lots of information to the client and provide a lot of useful context.

	This is an advanced tool for developers, and only the first project in a series of many.
	However, it was also designed to be an extremely simple project that should require no modifications.

	For more TL;DR, please see this thread:
	https://www.elitepvpers.com/forum/sro-coding-corner/4926697-inspirations-sro_devkit.html

### Usage
	Simply run the program and type in the pid (dec/hex) of the sro_client to use

	You can also run the program from the command line and pass in the pid (dec/hex) as an argument
		Example: GFXRC_Generator.exe 777
		Example: GFXRC_Generator.exe 0x404

### Source
	This is a C# .Net 5 project
	
	https://gitlab.com/pushedx/gfxrc_generator

	The code is heavily commented, so a lot of additional information can be found inside.

	This project was intentionally designed and is being posted as a standalone tool.
	That means certain "common" code is directly integrated to keep things as simple as possible.

	Certain design decisions are being made with the code in order to achieve a much larger goal of advancing community development.
	When developing massive, complex projects, certain tradeoffs have to be made to ensure as much accessibility and participation is possible.
	This is just one foundational cornerstone project, so the big picture might be hard to visualize until more tools are released.

### Dependencies
	Make sure to "Restore NuGet Packages" in Visual Studio!

	Newtonsoft.Json 13.0.1
	SharpDisasm 1.1.11

### FAQ
    Q.	Can the tool output more than C# structure files?
    A.	There's no reason to. I am using C# as a metalanguage to be able to generate other language outputs via another project.

    Q.	What should I expect to see in the console from running the program?
    A.	Here's an example run log: https://gist.github.com/pushedx/11f43a864993b75993a4cb8d99913a72

    Q.	How do I actually use the generated classes?
    A.	That will be covered in the next project!
		If you want a hint, find static pointers to read any of the 'composite' structures from.