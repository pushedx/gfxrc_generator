﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace Common
{
    public static class Memory
    {
        public static string ReadNullTerminatedStringA(this Process process, UIntPtr address)
        {
            // We should not really be reading any unknown strings longer than 4KB (I think?)
            const int ExpectedMaxStringSize = 4096;

            // First, query the region to make sure we don't read outside it and throw an exception
            var szLength = (UIntPtr) Unsafe.SizeOf<Interop.MEMORY_BASIC_INFORMATION>();
            Interop.MEMORY_BASIC_INFORMATION mbi;
            var szResult = Interop.VirtualQueryEx(process.SafeHandle,
                address,
                out mbi,
                szLength);
            if (szResult != szLength)
                throw new Exception($"'VirtualQueryEx' failed with error [0x{Marshal.GetLastWin32Error()}]");

            // We want to read from 'address' to the end of the region or 'ExpectedMaxStringSize', whichever is smaller
            var szToRead = (UIntPtr) ((mbi.BaseAddress.ToUInt64() + mbi.RegionSize.ToUInt64()) - address.ToUInt64());
            if (szToRead.ToUInt64() > ExpectedMaxStringSize)
                szToRead = (UIntPtr) ExpectedMaxStringSize;

            var bytes = new byte[szToRead.ToUInt32()];

            if (!Interop.ReadProcessMemory(process.SafeHandle, address, bytes, szToRead, out var szRead))
                throw new Exception($"'ReadProcessMemory' failed with error [0x{Marshal.GetLastWin32Error()}]");

            if (szRead != szToRead)
                throw new Exception(
                    $"'ReadProcessMemory' read only [{szRead.ToUInt32()} / {szToRead.ToUInt32()}] bytes");
            for (var idx = 0; idx < bytes.Length; ++idx)
            {
                if (bytes[idx] == 0)
                    return Encoding.ASCII.GetString(bytes, 0, idx);
            }

            // If we get here, we're either trying to read non-string memory,
            // or we need to read more than 'ExpectedMaxStringSize' for a string.
            // Increase 'ExpectedMaxStringSize' and re-run.
            throw new Exception($"Did not find a null terminator within {szToRead.ToUInt32()} bytes!");
        }
    }
}
