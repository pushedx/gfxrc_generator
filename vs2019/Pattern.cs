﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Common
{
    public class Pattern
    {
        public ushort[] Signature { get; }

        public static ushort[] CompilePattern(string pattern)
        {
            var patternSb = new StringBuilder();
            foreach (var ch in pattern)
            {
                if (!char.IsWhiteSpace(ch))
                {
                    patternSb.Append(ch);
                }
            }

            if (patternSb.Length % 2 != 0)
                throw new Exception($"Invalid signature length for \"{pattern}\"");

            var result = new ushort[patternSb.Length / 2];

            var tempSb = new StringBuilder("##");
            var resultIdx = 0;
            for (var idx = 0; idx < patternSb.Length; idx += 2)
            {
                if (patternSb[idx] == '?' || patternSb[idx + 1] == '?')
                {
                    if (patternSb[idx] != patternSb[idx + 1])
                        throw new Exception($"Invalid wildcard group for \"{pattern}\"");

                    result[resultIdx++] = 0xFFFF;
                }
                else
                {
                    tempSb[0] = patternSb[idx];
                    tempSb[1] = patternSb[idx + 1];
                    result[resultIdx++] = byte.Parse(tempSb.ToString(), NumberStyles.HexNumber);
                }
            }

            return result;
        }

        public Pattern(string pattern)
        {
            Signature = CompilePattern(pattern);
        }

        public List<int> Find(byte[] workspace)
        {
            // NOTE: We return ints to avoid various signed/unsigned type mismatches.
            // In reality, trying to work with 2gb+ blocks of data is impractical with
            // standard code, so while we could be using an uint instead, there's no
            // real reason to.

            List<int> results = new();
            
            // NOTE: This logic matches results byte by byte, and not pattern by pattern.
            // For example, if the input workspace was "aaa" and the pattern to match was
            // "aa", two results would be found, [aa]a and then a[aa]. Most code where I use
            // my pattern matching needs things to work this way.

            for (var idx = 0; idx < workspace.Length; ++idx)
            {
                // The index we need to check in the signature
                var check_index = 0;

                if (Signature[check_index] == 0xFFFF ||
                    workspace[idx] == Signature[check_index])
                {
                    // We've matched the first byte in the signature
                    var match_count = 1;
                    ++check_index;

                    for (var idy = idx + 1; idy < workspace.Length; ++idy)
                    {
                        // Make sure we've not reached the end of the byte signature
                        if (check_index >= Signature.Length)
                            break;

                        // See if the current byte matches the current signature byte
                        if (Signature[check_index] != 0xFFFF &&
                            workspace[idy] != Signature[check_index])
                            break;

                        // The current byte matched the signature
                        ++check_index;
                        ++match_count;
                    }

                    // If we've matched all the bytes, we've found an instance
                    if (match_count == Signature.Length)
                        results.Add(idx);
                }
            }

            return results;
        }
    }
}
