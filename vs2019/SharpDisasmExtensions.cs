﻿using System;
using System.Collections.Generic;
using SharpDisasm;

namespace Common
{
    public static class SharpDisasmExtensions
    {
        public static UIntPtr ResolveAddress(this Instruction insn, int opIndex)
        {
            // We have to do things this way for x86 compatibility. Since we're brute force disasming the client
            // in a linear fashion to search for calls, it's possible we decode a call that is not really a call
            // but rather 0xE8 followed by 4 random bytes of data. The call's destination can lie outside of the
            // 32-bit address space, so casting to UIntPtr on x86 would throw an exception (this actually happens).
            // If we simply truncate the value, we risk false positives if the call resolves to a legitimate call
            // we're looking for (has happened to me in other projects) so instead we'll simply not resolve the call
            // and return 0. We can't throw an exception either, because this issue is normal when disassembling 
            // arbitrary bytes that might not be code (as is the case when you disassemble the jump table for a switch)

            var address = (long) insn.PC + insn.Operands[opIndex].LvalSDWord;
            if (address < 0 || address > uint.MaxValue)
                return UIntPtr.Zero;
            return (UIntPtr) address;
        }

        public static uint GetOperandValueU32(this Instruction insn, int opIndex)
        {
            return (uint) insn.Operands[opIndex].Value;
        }

        public static List<Instruction> ExtractUntil(byte[] image, UIntPtr baseAddress, UIntPtr extractAddress,
            Func<Instruction, bool> shouldStop)
        {
            List<Instruction> insns = new();
            using (var disasm = new Disassembler(image, ArchitectureMode.x86_32,
                extractAddress.ToUInt32(), true,
                Vendor.Intel, extractAddress.ToUInt32() - baseAddress.ToUInt32()))
            {
                foreach (var insn in disasm.Disassemble())
                {
                    insns.Add(insn);
                    if (shouldStop(insn))
                        break;
                }
            }

            return insns;
        }
    }
}
