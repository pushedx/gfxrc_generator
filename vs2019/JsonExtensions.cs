﻿using Newtonsoft.Json;

namespace Common
{
    public static class JsonExtensions
    {
        public static string ToJson(this object @object)
        {
            return JsonConvert.SerializeObject(@object, Formatting.Indented);
        }

        public static T FromJson<T>(this string @string)
        {
            return JsonConvert.DeserializeObject<T>(@string);
        }
    }
}
