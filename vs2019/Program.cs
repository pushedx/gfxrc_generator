﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Common;
using Shared;
using SharpDisasm;

namespace GFXRC_Generator
{
    class Program
    {
        private static bool PauseOnExit { get; set; }

        static void ShowUsage()
        {
            Console.WriteLine("Usage: GFXRC_Generator.exe");
            Console.WriteLine("\tAccepts user console input for the process id");
            Console.WriteLine("");

            Console.WriteLine("Usage: GFXRC_Generator.exe [pid]");
            Console.WriteLine("\tUses the specified process id");
            Console.WriteLine("");
        }

        static void AttachToProcess(int pid)
        {
            Dictionary<UIntPtr, string> x32dbg_labels = new();
            List<GFXRuntimeClass> gfxRuntimeClasses = new();
            Dictionary<string, GFXRuntimeClass> nameToGFXRuntimeClass = new();
            Dictionary<uint, GFXRuntimeClass> addressToGFXRuntimeClass = new();
            Dictionary<string, UIntPtr> gfxRuntimeClassCtors = new();

            var RegisterGFXRuntimeClass = UIntPtr.Zero;
            var NewGFXRuntimeClass = UIntPtr.Zero;

            using var process = Process.GetProcessById(pid);

            var md5 = process.HashMD5();

            var baseOutputDir = Path.Combine("output", md5);

            var classOutputDir = Path.Combine(baseOutputDir, "GFX_RUNTIME_CLASS");
            Directory.CreateDirectory(Path.Combine(classOutputDir, "data"));
            Directory.CreateDirectory(Path.Combine(classOutputDir, "composite"));

            // SRO is a 15+ year old 32-bit game that doesn't have ASLR. Rather than have a bunch of
            // generic code for processing exe sections and such, we're just going to support clients
            // that follow a standard layout, where the base address is the 0x400000 and the
            // code section is at 0x401000.
            //
            // In doing so, the only clients that won't be supported, are clients that have made
            // very specific changes to break this tool. Packed clients are still technically supported
            // if the unpacking process has completed, which requires the client to be running in memory.
            // That is why this tool supports "attaching" to a process ID, rather than trying to open an
            // exe or create the process itself!
            if (process.MainModule.BaseAddress.ToInt32() != 0x400000)
                throw new Exception("The base address of the process is no 0x400000");

            var info = new StringBuilder();
            info.AppendLine($"[{process.Id}] {process.MainModule.FileName}");
            info.AppendLine($"\tImageSize: 0x{process.MainModule.ModuleMemorySize:X}");
            info.AppendLine($"\tMD5: {md5}");

            // Since we save the generated files by MD5, it becomes hard to track which folder is for what 
            // version, so we'll save a log to easily figure it out. It's ideal to rename the clients per-version
            // so that info is saved as well!
            File.WriteAllText(Path.Combine(baseOutputDir, "GFXRC_Generator.log"), info.ToString());

            Console.WriteLine(info.ToString());

            // First, query the process's text section
            var szLength = (UIntPtr)Unsafe.SizeOf<Interop.MEMORY_BASIC_INFORMATION>();
            Interop.MEMORY_BASIC_INFORMATION mbi;
            var szResult = Interop.VirtualQueryEx(process.SafeHandle, (UIntPtr)0x401000, out mbi, szLength);
            if (szResult != szLength)
                throw new Exception($"'VirtualQueryEx' failed with error [0x{Marshal.GetLastWin32Error()}]");

            var code = new byte[mbi.RegionSize.ToUInt32()];

            // Read the code section and make sure all bytes were read
            var szToRead = mbi.RegionSize;
            if (!Interop.ReadProcessMemory(process.SafeHandle, mbi.BaseAddress, code, szToRead, out var szRead))
                throw new Exception($"'ReadProcessMemory' failed with error [0x{Marshal.GetLastWin32Error()}]");

            if (szRead != szToRead)
                throw new Exception($"'ReadProcessMemory' read [{szRead.ToUInt32()} / {szToRead.ToUInt32()}] bytes");

            // This pattern should work across all base versions of SRO really...
            // 6A 00        | push 0x0
            // 6A 04        | push 0x4
            // 6A 00        | push 0x0
            // 6A 00        | push 0x0
            // 6A 00        | push 0x0
            // 68 E4D9E200  | push 0xE2D9E4 | "CObj"
            // B9 EC881A01  | mov ecx, gfxrc_CObj
            // E8 B7306C00  | call <RegisterGFXRuntimeClass>
            // C3           | ret
            var pattern_RegisterGFXRuntimeClass_CObj =
                new Pattern("6A 00 6A 04 6A 00 6A 00 6A 00 68 ?? ?? ?? ?? B9 ?? ?? ?? ?? E8 ?? ?? ?? ?? C3");

            var results = pattern_RegisterGFXRuntimeClass_CObj.Find(code);
            if (results.Count != 1)
                throw new Exception($"Expected 1 result, but {results.Count} results were found.");

            var RegisterGFXRuntimeClass_CObj = (UIntPtr)(mbi.BaseAddress.ToUInt32() + results[0]);
            Console.WriteLine($"RegisterGFXRuntimeClass_CObj: 0x{RegisterGFXRuntimeClass_CObj:X}");

            // Now disassemble the function to get the address of the 'RegisterGFXRuntimeClass' function
            using (var disasm = new Disassembler(code, ArchitectureMode.x86_32, RegisterGFXRuntimeClass_CObj.ToUInt64(),
                true,
                Vendor.Intel, RegisterGFXRuntimeClass_CObj.ToUInt64() - 0x401000))
            {
                foreach (var insn in disasm.Disassemble())
                {
                    if (insn.Mnemonic == SharpDisasm.Udis86.ud_mnemonic_code.UD_Iret ||
                        insn.Mnemonic == SharpDisasm.Udis86.ud_mnemonic_code.UD_Iint3)
                        break;

                    if (insn.Mnemonic == SharpDisasm.Udis86.ud_mnemonic_code.UD_Icall)
                    {
                        RegisterGFXRuntimeClass = insn.ResolveAddress(0);
                        break;
                    }
                }
            }

            // This will usually either happen from a pserver breaking it, or a user is running the client in a debugger,
            // and sets a breakpoint inside the function on a non-wildcard byte.
            if (RegisterGFXRuntimeClass == UIntPtr.Zero)
            {
                throw new Exception(
                    "Could not find a call to the 'RegisterGFXRuntimeClass' function.");
            }

            Console.WriteLine($"RegisterGFXRuntimeClass: 0x{RegisterGFXRuntimeClass.ToUInt32():X}");
            Console.WriteLine();

            x32dbg_labels.Add(RegisterGFXRuntimeClass, "Register_GFXRuntimeClass");

            // Now look for all calls to 'RegisterGFXRuntimeClass'
            using (var disasm = new Disassembler(code, ArchitectureMode.x86_32, mbi.BaseAddress.ToUInt64(), true,
                Vendor.Intel))
            {
                List<Instruction> insns = new();

                int callCount = 0;
                foreach (var insn in disasm.Disassemble())
                {
                    // Sliding window buffer of insns so we can process the function easily. In 32-bit mode,
                    // if we were to try to load every instruction, we could possibly hit the 32-bit memory limit,
                    // as has been observed in a C++ version of this program (that optimized program used 3.5GB).
                    insns.Insert(0, insn);
                    if (insns.Count > 8)
                        insns.RemoveAt(insns.Count - 1);

                    if (insn.Mnemonic != SharpDisasm.Udis86.ud_mnemonic_code.UD_Icall)
                        continue;

                    var target = insn.ResolveAddress(0);
                    if (target != RegisterGFXRuntimeClass)
                        continue;

                    var callee = (insn.PC - (uint)insn.Length);
                    Console.WriteLine($"[{++callCount}] Call to 'RegisterGFXRuntimeClass' @ 0x{callee:X}");

                    GFXRuntimeClass instance = new();
                    instance.UnknownValue = insns[7].GetOperandValueU32(0);
                    instance.TotalSize = insns[6].GetOperandValueU32(0);
                    instance.ParentAddress = insns[5].GetOperandValueU32(0);
                    instance.UnknownFunction = insns[4].GetOperandValueU32(0);
                    instance.Allocate = insns[3].GetOperandValueU32(0);
                    var instanceNamePtr = insns[2].GetOperandValueU32(0);
                    instance.Name = process.ReadNullTerminatedStringA((UIntPtr)instanceNamePtr);
                    instance.Address = insns[1].GetOperandValueU32(1);

                    Console.WriteLine(
                        $"\t{instance.Name} (0x{instance.TotalSize:X} bytes) [0x{instance.Address:X}]");

                    gfxRuntimeClasses.Add(instance);
                    nameToGFXRuntimeClass.Add(instance.Name, instance);
                    addressToGFXRuntimeClass.Add(instance.Address, instance);

                    // The function insns are backwards because we're matching the call at the end of the function
                    var functionStart = (UIntPtr)(insns[7].PC - (uint)insns[7].Length);
                    x32dbg_labels.Add(functionStart, $"Register_GFXRuntimeClass_{instance.Name}");

                    x32dbg_labels.Add((UIntPtr)instance.Address, $"GFXRuntimeClass_{instance.Name}");

                    // CObj can't be allocated, so we can't label an invalid address
                    if (instance.Allocate == 0)
                        continue;

                    x32dbg_labels.Add((UIntPtr)instance.Allocate, $"Allocate_GFXRuntimeClass_{instance.Name}");

                    // In each allocate function, the code pattern is simply memory allocation followed by the ctor,
                    // so we can label the ctors for more analysis.
                    var funcInsns = SharpDisasmExtensions.ExtractUntil(code, mbi.BaseAddress,
                        (UIntPtr)instance.Allocate, (localInsn) =>
                        {
                            // Stop after the first return, since we've extracted enough by then. The function
                            // itself might span through another ret, but it doesn't matter, since we'd have to
                            // logically disassemble the function to know when we should stop. For example, in CSRO-R,
                            // a function only has 1 ret, and in VSRO, a function has no int3 padding. Brute force
                            // disassembling like this is not an exact science, so we have to find what works even if it's
                            // not perfect.
                            if (localInsn.Mnemonic == SharpDisasm.Udis86.ud_mnemonic_code.UD_Iret)
                                return true;

                            return false;
                        });

                    List<UIntPtr> calls = new();
                    foreach (var funcInsn in funcInsns)
                    {
                        if (funcInsn.Mnemonic == SharpDisasm.Udis86.ud_mnemonic_code.UD_Icall)
                        {
                            calls.Add(funcInsn.ResolveAddress(0));
                        }
                    }

                    // We should have exactly two calls
                    if (calls.Count != 2)
                    {
                        throw new Exception(
                            $"[0x{instance.Allocate:X}] AllocateGFXRuntimeClass_{instance.Name} has an unexpected function format. {calls.Count} calls when only 2 were expected!");
                    }

                    // The first call is the 'new' for this object type
                    if (NewGFXRuntimeClass == UIntPtr.Zero)
                    {
                        NewGFXRuntimeClass = calls[0];
                        Console.WriteLine($"NewGFXRuntimeClass: 0x{NewGFXRuntimeClass.ToUInt32():X}");
                        x32dbg_labels.Add(NewGFXRuntimeClass, "New_GFXRuntimeClass");
                    }

                    // Sanity check the function format
                    if (NewGFXRuntimeClass != calls[0])
                    {
                        throw new Exception(
                            $"[0x{instance.Allocate:X}] Allocate_GFXRuntimeClass_{instance.Name} has an unexpected function format. The first call is to [0x{calls[0].ToUInt32():X}] rather than [0x{NewGFXRuntimeClass.ToUInt32():X}]");
                    }

                    // We need to store these first, and then process them afterwards because some ctors can be reused based on
                    // class hierarchy, so we'll want to label them based on depth rather than order we find them
                    gfxRuntimeClassCtors.Add(instance.Name, calls[1]);
                }

                Console.WriteLine();
            }

            // Build class hierarchy layout
            foreach (var gfxRuntimeClass in gfxRuntimeClasses)
            {
                // Figure out the real size of the object
                gfxRuntimeClass.Size = gfxRuntimeClass.TotalSize;
                if (addressToGFXRuntimeClass.TryGetValue(gfxRuntimeClass.ParentAddress, out var parent))
                    gfxRuntimeClass.Size -= parent.TotalSize;

                // Everything should be aligned to 4 bytes
                if (gfxRuntimeClass.Size % 4 != 0)
                {
                    throw new Exception(
                        $"{gfxRuntimeClass.Name}'s size (0x{gfxRuntimeClass.Size:X}) is not divisible by 4!");
                }

                // Traverse the parent hierarchy to the root
                while (parent != null)
                {
                    gfxRuntimeClass.Hierarchy.Add(parent.Name);
                    var addr = parent.ParentAddress;
                    if (!addressToGFXRuntimeClass.TryGetValue(addr, out parent))
                    {
                        // I think this is correct, we should throw if we can't find class info for a non-null parent
                        if (addr != 0)
                            throw new Exception($"Could not find runtime class information for [0x{addr:X}]");
                    }
                }
            }

            Dictionary<string, int> name_to_depth = new();

            // Calculate the max hierarchy depth
            var maxDepth = 0;
            foreach (var gfxRuntimeClass in gfxRuntimeClasses)
            {
                var depth = gfxRuntimeClass.Hierarchy.Count;
                if (depth > maxDepth)
                    maxDepth = depth;
                name_to_depth[gfxRuntimeClass.Name] = depth;
            }

            // Debug hierarchy from top to bottom
            for (var depth = 0; depth <= maxDepth; ++depth)
            {
                Console.WriteLine($"[Depth: {depth}]");
                foreach (var gfxRuntimeClass in gfxRuntimeClasses.Where(gfxrc => gfxrc.Hierarchy.Count == depth))
                {
                    Console.WriteLine(
                        $"\t{gfxRuntimeClass.Name} (0x{gfxRuntimeClass.Size:X} bytes | 0x{gfxRuntimeClass.TotalSize:X} bytes total)");
                    Console.Write("\t\t");
                    for (var d = 0; d < gfxRuntimeClass.Hierarchy.Count; ++d)
                    {
                        Console.Write($"{gfxRuntimeClass.Hierarchy[d]}");
                        if ((d + 1) != gfxRuntimeClass.Hierarchy.Count)
                            Console.Write(" -> ");
                    }

                    Console.WriteLine();
                }
            }

            Console.WriteLine();

            // Now we can assign ctors based on hierarchy depth
            for (var depth = 0; depth <= maxDepth; ++depth)
            {
                foreach (var gfxRuntimeClass in gfxRuntimeClasses.Where(gfxrc => gfxrc.Hierarchy.Count == depth))
                {
                    // Skip CObj
                    if (gfxRuntimeClass.Allocate == 0)
                        continue;

                    var ctor = gfxRuntimeClassCtors[gfxRuntimeClass.Name];
                    if (!x32dbg_labels.TryAdd(ctor, $"Constructor_GFXRuntimeClass_{gfxRuntimeClass.Name}"))
                    {
                        Console.WriteLine(
                            $"[WARN] [0x{ctor:X}] Constructor_GFXRuntimeClass_{gfxRuntimeClass.Name} has already been added as \"{x32dbg_labels[ctor]}\"");

                        // This is a bit of sanity checking logic. This makes sure we don't incorrectly assign a
                        // constructor to a same depth class. So far, this should not happen in SRO, and since we're
                        // processing in depth order, the value should never be non-equal.
                        //
                        // For example consider the two layouts
                        //      A-B-C (calls ctor E)
                        //      A-B-D (calls ctor E)
                        //
                        // If we process C first, and label 'ctor E' as belonging to 'C', then we'll have a conflict 
                        // when we process 'D', since it too uses 'ctor E', and the labeling would incorrectly
                        // imply it only belongs to 'C". This doesn't actually happen, but the code is here to catch it if
                        // it ever did.
                        //
                        // This is how we can verify our ctor labeling logic is correct, and any conflicts detected below
                        // are normal, because a sub-hierarchy type is being constructed first via a function call, and then
                        // the actual type's ctor is embedded in the Allocate_ function itself, usually because it doesn't warrant
                        // its own call, so it gets optimized inline.
                        //

                        var myDepth = name_to_depth[gfxRuntimeClass.Name];

                        var otherName = x32dbg_labels[ctor].Replace("Constructor_GFXRuntimeClass_", "");
                        var otherDepth = name_to_depth[otherName];

                        if (myDepth <= otherDepth)
                        {
                            throw new Exception(
                                $"Constructor conflict detected. '{gfxRuntimeClass.Name}' @ depth {myDepth} comes at or before '{otherName}' @ depth {otherDepth}.");
                        }
                    }
                }
            }

            Console.WriteLine();

            // First: write out the raw data structs
            for (var depth = 0; depth <= maxDepth; ++depth)
            {
                foreach (var gfxRuntimeClass in gfxRuntimeClasses.Where(gfxrc => gfxrc.Hierarchy.Count == depth))
                {
                    StringBuilder sb = new();

                    sb.AppendLine("using System.Runtime.InteropServices;");
                    sb.AppendLine();

                    sb.AppendLine("namespace Native");
                    sb.AppendLine("{");

                    sb.AppendLine("\t[StructLayout(LayoutKind.Sequential, Pack = 1)]");
                    sb.AppendLine($"\tpublic struct {gfxRuntimeClass.Name}_Data");
                    sb.AppendLine("\t{");

                    // C# doesn't support 0-sized fixed buffers
                    /*if (gfxRuntimeClass.Size > 0)
                        sb.AppendLine($"\t\tpublic unsafe fixed byte _0000[0x{gfxRuntimeClass.Size:X}];");*/

                    // Reversing is way easier/faster in 32-bits thanks to 4 byte alignment by just assuming
                    // everything is an uint, and then fixing everything else. If we use the fixed byte method
                    // above, it's impossibly hard to do anything efficiently with the automatic format dumps since
                    // all your time is spent manually making a bunch of uints anyways
                    for (var offset = 0; offset < gfxRuntimeClass.Size; offset += 4)
                        sb.AppendLine($"\t\tpublic uint _{offset:X4}; // 0x{offset:X4}");

                    sb.AppendLine("\t}");
                    sb.AppendLine("}");

                    File.WriteAllText(Path.Combine(classOutputDir, "data", $"{gfxRuntimeClass.Name}_Data.cs"),
                        sb.ToString());
                }
            }

            // Second: write out the composite object structs
            for (var depth = 0; depth <= maxDepth; ++depth)
            {
                foreach (var gfxRuntimeClass in gfxRuntimeClasses.Where(gfxrc => gfxrc.Hierarchy.Count == depth))
                {
                    StringBuilder sb = new();

                    sb.AppendLine("using System.Runtime.InteropServices;");
                    sb.AppendLine();

                    sb.AppendLine("namespace Native");
                    sb.AppendLine("{");

                    // Saving the Allocation address breaks easy diffing between versions/updates,
                    // so it's disabled for now. We'll just use our x32dbg script to be able to jump to
                    // address easily once we learn the names instead.
                    //sb.AppendLine("\t/// <summary>");
                    //sb.AppendLine($"\t/// Allocate: 0x{gfxRuntimeClass.Allocate:X}");
                    //sb.AppendLine("\t/// </summary>");

                    // NOTE: The naming convention is very important. Since composite objects just hold a bunch of data
                    // types (which devs will change the format of manually), it's important we don't break the composite
                    // structs if the data objects change size between game versions. This way, the main composite accessing
                    // code will be stable name wise, and since each data object is unique since its a rtti-like hierarchy,
                    // devs won't ever have to touch the files again.

                    sb.AppendLine(
                        $"\t[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x{gfxRuntimeClass.TotalSize:X})]");
                    sb.AppendLine($"\tpublic struct {gfxRuntimeClass.Name}");
                    sb.AppendLine("\t{");
                    uint offset = 0;
                    for (var idx = 0; idx < gfxRuntimeClass.Hierarchy.Count; ++idx)
                    {
                        // Hierarchy is bottom up, so start from the top (back) to list it top down
                        var parentName = gfxRuntimeClass.Hierarchy[gfxRuntimeClass.Hierarchy.Count - idx - 1];
                        var parentClass = nameToGFXRuntimeClass[parentName];
                        var parentSize = parentClass.Size;

                        // C# doesn't handle 0 byte structs (adds 1 byte), so we have to comment out 0 sized types
                        sb.AppendLine(
                            $"\t\t{(parentSize == 0 ? "//" : "")}public {parentName}_Data {(parentSize != 0 ? "" : "Virtual_")}{parentName.Substring(1)}; // 0x{offset:X4} (0x{parentSize:X} bytes)");

                        offset += parentSize;
                    }

                    // C# doesn't handle 0 byte structs (adds 1 byte), so we have to comment out 0 sized types
                    sb.AppendLine(
                        $"\t\t{(gfxRuntimeClass.Size == 0 ? "//" : "")}public {gfxRuntimeClass.Name}_Data {gfxRuntimeClass.Name.Substring(1)}; // 0x{offset:X4} (0x{gfxRuntimeClass.Size:X} bytes)");

                    sb.AppendLine("\t}");
                    sb.AppendLine("}");

                    File.WriteAllText(Path.Combine(classOutputDir, "composite", $"{gfxRuntimeClass.Name}.cs"),
                        sb.ToString());
                }
            }

            // Write out the x32dbg label script
            if (x32dbg_labels.Any())
            {
                File.WriteAllText(Path.Combine(baseOutputDir, "x32dbg-labelscript.txt"),
                    Utility.GenerateLabelScript_x32dbg(x32dbg_labels));
            }

            // Write out the rtti info as a json file for easy loading between projects
            File.WriteAllText(Path.Combine(baseOutputDir, "GFX_RUNTIME_CLASS.json"), gfxRuntimeClasses.ToJson());
        }

        static int MainWrapper(string[] args)
        {
            if (args.Length > 1)
            {
                ShowUsage();
                return 1; // EXIT_FAILURE
            }

            string arg;
            if (args.Length == 1)
            {
                arg = args[0].Trim();
            }
            else
            {
                Console.Write("Please enter the game's process id (hex or dec): ");
                arg = Console.ReadLine()?.Trim();

                // When no argument is passed to the program, we assume interactive mode, so
                // pause the program on exit so the user can see if there's any issues or know
                // everything worked out.
                PauseOnExit = true;
            }

            if (string.IsNullOrEmpty(arg))
            {
                ShowUsage();
                return 1; // EXIT_FAILURE
            }

            int pid;
            try
            {
                if (arg.StartsWith("0x"))
                {
                    pid = Convert.ToInt32(arg, 16);
                }
                else
                {
                    pid = int.Parse(arg, NumberStyles.Integer, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to parse the input string [{arg}] Reason: {ex.Message}");
            }

            var sw = Stopwatch.StartNew();
            AttachToProcess(pid);
            sw.Stop();

            Console.WriteLine($"Successfully completed in {sw.Elapsed}");

            return 0; // EXIT_SUCCESS
        }

        static int Main(string[] args)
        {
            // NOTE: This program uses Win32 standardized return values (0 is success, non-0 is failure)
            // https://docs.microsoft.com/en-gb/dotnet/api/system.environment.exitcode?view=net-5.0

            int result;
            try
            {
                result = MainWrapper(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                result = 1; // EXIT_FAILURE
            }

            if (PauseOnExit)
            {
                Console.WriteLine("Please press Enter/Return to exit...");
                Console.ReadLine();
            }

            return result;
        }
    }
}
