﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;

namespace Common
{
    public static class Extensions
    {
        public static string HashMD5(this Process process)
        {
            using var md5 = MD5.Create();
            using var stream = File.OpenRead(process.MainModule.FileName);
            return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToUpperInvariant();
        }
    }
}
