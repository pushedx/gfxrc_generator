﻿using System.Collections.Generic;

namespace Shared
{
    public class GFXRuntimeClass
    {
        public uint UnknownValue { get; set; }

        public uint TotalSize { get; set; }

        public uint ParentAddress { get; set; }

        public uint UnknownFunction { get; set; }

        public uint Allocate { get; set; }

        public string Name { get; set; }

        public uint Address { get; set; }

        public uint Size { get; set; }

        public List<string> Hierarchy { get; set; } = new();
    }
}
