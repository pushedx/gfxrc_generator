﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public static class Utility
    {
        public static string GenerateLabelScript_x32dbg(Dictionary<UIntPtr, string> labels)
        {
            // NOTE: This version is tailored to SRO and the base address being 0x400000
            // Otherwise we'd use the format: labelset mod.main() + 0xOffset, \"label\"

            var sb = new StringBuilder();
            foreach (var kvp in labels.OrderBy(kvp => kvp.Value))
            {
                // We shouldn't have generated any 0 address labels, but double check
                if (kvp.Key == UIntPtr.Zero)
                    throw new Exception($"Cannot create label for '{kvp.Value}' because the address is 0!");

                sb.AppendLine($"labelset 0x{kvp.Key.ToUInt64():X}, \"{kvp.Value}\"");
            }

            sb.AppendLine("log \"Done!\"");
            sb.AppendLine("ret");

            return sb.ToString();
        }
    }
}
